//-------------  Runs getdata function once the the DOM is fully loaded and parsed
window.addEventListener('DOMContentLoaded', () => {
    getdata();
});


//------------- GET data from JSON file and displays on page
function getdata() {
    var i = 0; // used to loop through json results to dispay unique data

    fetch('link-data.json')
    .then((response) => response.json())
    .then((data) => {
        let output = '';
        data.forEach(function(){
            // HTML posted for each link result
            output += `
            <div class="link-holder-outer">
                <div class="link-holder">
                    <span id="website-title">${data[i].title} </span>
                    <span id="website-url"><a target="blank" href="${data[i].url}">${data[i].url}</a></span>
                    <span class="link-options"><img class="options-img" onclick="options();" src="img/options.png"></span>
                </div>
                <div id="website-description">${data[i].description}</div>
            </div>`;
        i ++;
    })
        document.getElementById('output').innerHTML = output;
        pagination(); //fires Pagination function
        console.log(data);
    })
        .catch((err) => console.log(err)) // If an error is found console log error    
}


//------------- Pagination - dynamically created to serve all lengths of data
function pagination() {
       pageSize = 20; //Set how many items to display per page

       var pageCount =  $(".link-holder-outer").length / pageSize; //finds out how many pages need to be displayed
       for(var i = 0 ; i<pageCount;i++){
           
       $("#pagin").append('<li><a href="#">'+(i+1)+'</a></li> '); //dynamically creates page numbers to navigate
       }
           $("#pagin li").first().find("a").addClass("current")
       showPage = function(page) {
           $(".link-holder-outer").hide();
           $(".link-holder-outer").each(function(n) {
               if (n >= pageSize * (page - 1) && n < pageSize * page)
                   $(this).show();
           });        
       }
       showPage(1);
       $("#pagin li a").click(function() {
           $("#pagin li a").removeClass("current");
           $(this).addClass("current");
           showPage(parseInt($(this).text())) 
       }); 
}


//------------- fired once the a new link form is submitted
function form_submit() {
    // Gets vlaues from 'add new link' form
    var name_value = document.getElementById('name_value').value;
    var url_value = document.getElementById('url_value').value;
    var description_value = document.getElementById('description_value').value;

    confirm_box(name_value, url_value, description_value); //confirmation box call

    // Push data to JSON file
    fetch('post-request.php', {
    method:'POST',
    headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-type':'application/json'
    },
    body:JSON.stringify({
        id:"",
        title:"${name_value}",
        url:"${url_value}",
        description:"${description_value}"
        })
    })
    .catch((error) => console.error(error));
} 


    // Create a popup box confiming the users link data saved
    function confirm_box(name_value, url_value, description_value) {
        var currentCallback;

        // override default browser alert
        window.alert = function(msg, callback){
        $('.message').text(msg);
        $('.customAlert').css('animation', 'fadeIn 0.3s linear');
        $('.customAlert').css('display', 'inline');
        setTimeout(function(){
            $('.customAlert').css('animation', 'none');
        }, 300);
        currentCallback = callback;
        }
        
        $(function(){
        // add listener for when our confirmation button is clicked
            $('#alertConfirmButton').click(function(){
            $('.customAlert').css('animation', 'fadeOut 0.3s linear');
            setTimeout(function(){
            $('.customAlert').css('animation', 'none');
                $('.customAlert').css('display', 'none');
            }, 300);
            currentCallback();
        })

        alert('Bookmark saved. \n\nTitle: ' + name_value + "\n\nURL: " + url_value + '\n\nDescription: ' + description_value, function(){
            // console.log("alert button fired");       
        });
    });
    }


    //------------- options button
    function options() {
        console.log("options function called");
        //This function would create a tooltip style box asking users if they wanted to add edit or delete that link. 
        //From there requests would go to the appropriate function to comeplete the task
    }